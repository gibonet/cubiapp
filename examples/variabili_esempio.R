if(interactive()){
  library(shiny)
  library(cubiapp)
  
  ui <- fluidPage(
    datiUI("cubo", choices = c("Nessuna selezione", "cubi_RSS_01", "cubi_RSS_02")), 
    variabiliUI("variabili"),

    verbatimTextOutput("out")
  )
  
  server <- function(input, output, session) {
    cubo_s <- datiServer("cubo")
    v <- variabiliServer("variabili", cubo_s)
    
    output$out <- renderPrint({
      str(v())
    })
  }
  shinyApp(ui, server)
}
