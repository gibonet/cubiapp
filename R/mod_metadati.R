# Modulo 'metadati'

# Generazione di un bottone che chiama un showModal(modalDialog(...)) con i 
# metadati di un cubo

# Parte UI

#' @title modulo metadati
#' 
#' @description Genera un \code{\link{actionButton}} che apre una finestra con i metadati di un cubo.
#' 
#' @param id id del modulo.
#' 
#' @export
#' 
#' @name modulo-metadati
#' 
#' @example examples/metadati_esempio.R
metadatiUI <- function(id) {
  ns <- NS(id)
  
  uiOutput(ns("metadati"))
}



# Parte server

#' @param id id del modulo.
#' @param metadati un vettore character \code{reactive} con i metadati di un cubo. Per esempio può essere il risultato di \code{\link{dati3Server}} (elemento metadati della lista di output)
#' 
#' @export
#' 
#' @rdname modulo-metadati
metadatiServer <- function(id, metadati = reactive(NULL)) {
  stopifnot(is.reactive(metadati))
  
  moduleServer(id, function(input, output, session){
    ns <- session$ns
    
    output[["metadati"]] <- renderUI({
      req(metadati())
      actionButton(ns("meta_bottone"), label = "", icon = icon("question"), 
                   color = "warning", size = "sm")
    })
    
    observeEvent(input$meta_bottone, {
      showModal(shiny::modalDialog(
        HTML(paste(metadati(), collapse = "<br>")), 
        easyClose = TRUE, footer = modalButton("Chiudi")
      ))
    })
    
    
  }) # fine moduleServer

}
  