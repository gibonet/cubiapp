#' Funzione per far partire l'app del pacchetto cubiapp (terza variante)
#' 
#' @param choices vettore character con i nomi dei cubi da scegliere. Può anche essere una lista (vedi la documentaione dello stesso argomento in \code{\link{selectInput}}). Il valore predefinito è `lista_scelte`, una lista con i nomi dei cubi del pacchetto cubustat raggruppati per fonte.
#' @param label l'etichetta da dare al \code{downloadButton} (valore predefinito: "Scarica la tabella (xlsx)").
#' @param valore_tabella nome della colonna da usare per il \code{data.table::dcast} (corrisponde all'argomento \code{value.var} da passare a \code{data.table::dcast}). Valore predefinito: "valore".
#' @param width larghezza della \code{\link{selectInput}} (valore predefinito: "100\%").  
#' @param ... argomenti da passare a \code{\link{renderTable}}
#' 
#' 
#' @export
#' @example examples/cubi_app3_esempio.R
cubi_app3 <- function(choices = lista_scelte, label = "Scarica la tabella (xlsx)", 
                      valore_tabella = "valore", width = "100%", ...) {
  ui <- fluidPage(
    dati2UI("cubo", choices = choices, width = width),
    variabili2UI("variabili"),
    fluidRow(
      column(
        width = 4,
        sposta2UI("sposta")
      ),
      column(
        width = 8,
        scarica_xlsxUI("scarica_xlsx"),
        metadatiUI("metadati"),
        tabella2Output("tab")
      )
    )
  )
  
  server <- function(input, output, session){
    cubo_s <- dati3Server("cubo")
    
    v <- variabili2Server(
      id = "variabili", 
      dati = cubo_s$dati, 
      nomi_variabili = cubo_s$nomi_variabili, 
      livelli_variabili = cubo_s$livelli_variabili
    )
    
    f <- sposta2Server(
      id = "sposta",
      dati = cubo_s$dati,
      nomi_variabili = cubo_s$nomi_variabili
    )
    
    o <- tabella2Server(
      id = "tab", 
      dati = cubo_s$dati, 
      nomi_variabili = cubo_s$nomi_variabili, 
      lista_vars = v, 
      formula = f, 
      valore_tabella = valore_tabella,
      striped = TRUE, hover = TRUE, na = "",
      ...
    )
    
    scarica_xlsxServer("scarica_xlsx", dati = o, label = label)
    
    metadatiServer("metadati", metadati = cubo_s$metadati)
    
  }
  
  shinyApp(ui, server)
}
