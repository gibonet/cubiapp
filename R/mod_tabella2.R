# Modulo 'tabella2'

# Generazione della tabella filtrata e ridisposta

# Parte UI

#' @title modulo tabella2
#' 
#' @description Genera una tabella filtrata e ridisposta come output.
#' 
#' @param id id del modulo.
#' 
#' @return un output con la tabella filtrata e ridisposta
#' 
#' @export
#' 
#' @name modulo-tabella2
#' 
#' @example examples/app2_esempio.R
tabella2Output <- tabellaOutput
# tabellaOutput <- function(id){
#   ns <- NS(id)
#   
#   tableOutput(ns("tabella"))
# }




# Parte server
# Rispetto a tabellaServer c'è l'argomento nomi_variabili in più

#' @param id id del modulo
#' @param dati un cubo di dati \code{reactive}. Per esempio l'output della funzione \code{\link{datiServer}}.
#' @param nomi_variabili vettore character \code{reactive} con i nomi delle colonne di tipo factor del cubo di dati. Per esempio può essere il risultato di \code{\link{dati2Server}} (l'elemento `nomi_variabili` della lista).
#' @param lista_vars una lista \code{reactive} con i valori per filtrare la tabella. Per esempio l'output della funzione \code{\link{variabiliServer}}.
#' @param formula una formula \code{reactive} per la ridisposizione della tabella. Per esempio l'output della funzione \code{\link{spostaServer}}.
#' @param valore_tabella nome della colonna da usare per il \code{data.table::dcast} (corrisponde all'argomento \code{value.var} da passare a \code{data.table::dcast}). Valore predefinito: "valore".
#' @param ... argomenti da passare a \code{\link{renderTable}}
#' 
#' @export
#' 
#' @rdname modulo-tabella2
tabella2Server <- function(id, dati = reactive(NULL),
                           nomi_variabili = reactive(NULL),
                           lista_vars = reactive(NULL), 
                           formula = reactive(NULL),
                           valore_tabella = "valore",
                           ...){
  
  stopifnot(is.reactive(dati))
  stopifnot(is.reactive(nomi_variabili))
  stopifnot(is.reactive(lista_vars))
  stopifnot(is.reactive(formula))
  
  stopifnot(is.character(valore_tabella) && length(valore_tabella) == 1L)
  
  moduleServer(id, function(input, output, session){
    
    # Tabella filtrata
    tab <- reactive({
      setkeyv(dati(), cols = nomi_variabili())
      
      lista_scelte <- do.call(CJ, c(lista_vars(), sorted = FALSE))
      
      dati()[lista_scelte]
    })
    
    # Tabella ridisposta (dcast, reshaped)
    tab_cast <- reactive({
      if(tab()[ , .N] > 0 && all(vars_formula(formula()) %in% nomi_variabili())){
        dcast(tab(), formula = formula(), value.var = valore_tabella, sep = "||")
      }else{
        NULL
      }
    })
    
    # output$tabella <- reactive(res)
    output$tabella <- renderTable(tab_cast(), ...)
    
    # Ritorna anche la tabella filtrata e ridisposta come reactive
    # (così da poterla eventualmente usare successivamente...)
    reactive(tab_cast())
    
  }) # fine moduleServer
}