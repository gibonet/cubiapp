# Modulo 'scarica'

# Genera un bottone per scaricare una tabella reactive

#' @importFrom writexl write_xlsx

# Parte UI

#' @title modulo scarica_xlsx
#' 
#' @description Genera un bottone per scaricare una data.table reactive in xlsx (excel).
#' 
#' @param id id del modulo.
#' 
#' 
#' @export
#' 
#' @name modulo-scarica-xlsx
#' 
#' @example examples/scarica_xlsx_esempio.R
scarica_xlsxUI <- function(id){
  ns <- NS(id)
  
  uiOutput(ns("scarica"))
}




# Parte server

#' @param dati un cubo di dati \code{reactive}. Per esempio può essere il risultato di \code{\link{dati2Server}} (l'elemento `dati` della lista).
#' @param label l'etichetta da dare al \code{downloadButton} (valore predefinito: "Scarica (xlsx)").
#' 
#' @export
#' 
#' @rdname modulo-scarica-xlsx
scarica_xlsxServer <- function(id, dati = reactive(NULL), label = "Scarica la tabella (xlsx)"){
  
  stopifnot(is.reactive(dati))
  stopifnot(length(label) == 1L)
  
  moduleServer(id, function(input, output, session){
    ns <- session$ns
    
    output[["scarica"]] <- renderUI({
      req(dati())
      downloadButton(ns("scarica_tabella"), label = label)
    })
    
    output[["scarica_tabella"]] <- downloadHandler(
      filename = "tab.xlsx", # "tab01.csv.gz"
      content = function(file){
        # data.table::fwrite(dati(), file, compress = "auto", sep = sep, quote = TRUE)
        writexl::write_xlsx(dati(), path = file)
      }
    )
    
  }) # fine moduleServer
}