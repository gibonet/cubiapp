<!-- README.md is generated from README.Rmd. Please edit that file -->

# cubiapp

Pacchetto R con un’app shiny che permette di estrarre e ridisporre
informazioni dai cubi di dati del sito dell’Ustat. Il pacchetto contiene
anche dei moduli shiny che si potrebbero usare per creare altre app
shiny.

Due esempi di app possono essere chiamati con le funzioni `cubi_app()` e
`cubi_app2()`.

I moduli presenti sono i seguenti:  
- “dati” / “dati2” / “dati3” (`datiUI(id, choices)` e `datiServer(id)` /
`dati2UI(id, choices)` e `dati2Server(id)` / `dati3UI(id, choices)` e
`dati3Server(id)`)  
- “variabili” / “variabili2” (`variabiliUI(id)` e
`variabiliServer(id, dati)` / `variabili2UI(id)` e
`variabili2Server(id, dati, nomi_variabili, livelli_variabili)`)  
- “sposta” / “sposta2” (`spostaUI(id)` e `spostaServer(id, dati)` /
`sposta2UI(id)` e `sposta2Server(id, dati, nomi_variabili)`)  
- “tabella” / “tabella2” (`tabellaOutput(id)` e
`tabellaServer(id, dati, lista_vars, formula)` / `tabella2Output(id)` e
`tabella2Server(dati, nomi_variabili, lista_vars, formula, valore_tabella, ...)`)  
- “scarica” (`scaricaUI(id)` e
`scaricaServer(id, dati, label, sep, formato)`)  
- “scarica_xlsx” (`scarica_xlsxUI(id)` e
`scarica_xlsxServer(id, dati, label)`)

I moduli qui sopra possono essere combinati per creare delle app shiny.
Qualche esempio di app create con questi moduli sono le funzioni
`cubi_app()`, `cubi_app2()`, `cubi_app3()` e `cubi_app4()`.

# Installazione

Il pacchetto può essere installato con la funzione
`remotes::install_gitlab()`:

    if(!require(remotes)) install.packages("remotes")
    remotes::install_gitlab("gibonet/cubiapp")

# Esempio

``` r
library(shiny)
library(cubiapp)
#> Loading required package: cubustat
# options(browser = "google-chrome-stable")
options(shiny.launch.browser = TRUE)

# Cubi del pacchetto cubustat (data.tables)
ls("package:cubustat")
#>  [1] "cubi_POL_01"     "cubi_POL_02"     "cubi_POL_03"     "cubi_POL_04"    
#>  [5] "cubi_RIFOS_01"   "cubi_RIFOS_02"   "cubi_RSS_01"     "cubi_RSS_02"    
#>  [9] "cubi_RSS_03"     "cubi_SBA_01"     "cubi_SBA_02"     "cubi_SCEN_01"   
#> [13] "cubi_SDL_01"     "cubi_SEA_A_02"   "cubi_SEA_A_03"   "cubi_SEA_A_04"  
#> [17] "cubi_SEA_A_05"   "cubi_SEA_A_07"   "cubi_SEA_E_01"   "cubi_SFPI_01"   
#> [21] "cubi_SHIS_01"    "cubi_SHIS_02"    "cubi_STATENT_01" "cubi_STATENT_02"
#> [25] "cubi_STATENT_03" "cubi_STATENT_04" "cubi_STATPOP_01" "cubi_STATPOP_02"
#> [29] "cubi_UDSC_01"    "cubi_UDSC_02"
```

``` r
# Lanciare cubi_app4() con il cubo cubi_POL_01
cubi_app4(choices = "cubi_POL_01")
```

<img src="man/figures/README-unnamed-chunk-2-1.png" width="672" />

``` r
# Se si lancia cubi_app4() senza argomenti, si possono scegliere tutti i cubi
# inclusi nel pacchetto cubustat
cubi_app4()
```

<img src="man/figures/README-unnamed-chunk-2-2.png" width="672" />
