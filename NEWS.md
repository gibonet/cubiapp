
# cubiapp 0.0.9.9001

- aggiunto l'argomento `header` a `spostaServer()` e `sposta2Server()`, che va 
 nella chiamata a `sortable::bucket_list()`. Tolto `shiny::strong(shiny::h4(...)` 
 all'header di `sortable::bucket_list()`, che creava problemi con la versione di 
 `sortable` superiore a 0.4.4.  


# cubiapp 0.0.5

- modulo dati3: `dati3Server` restituisce, nella lista di reactive, un vettore
  di testo con i metadati di un cubo.  
- modulo metadati: `metadatiUI` e `metadatiServer` generano un bottone che apre
  i metadati del cubo selezionato.  
- nuova funzione/app: `cubi_app3()`. Rispetto a `cubi_app2()`, viene generato
  un bottone con un punto di domanda che apre il metadato del cubo scelto.  
  




# cubiapp 0.0.4

- aggiunta di alcuni elementi di metadato, come: `scelte2`, `lista_metadati`, 
  `titoli_cubi`, `fonti`, `fonti_lunghe`, `lista_cubi`, `lista_scelte`. Tutti questi elementi
  sono ricavati dal pacchetto `cubustat`.  
- aggiunti alcuni argomenti `dati2UI()`: label, width, size, selectize 
  (sono gli stessi argomenti della funzione `shiny::selectInput()`).  
- aggiunto l'argomento width a `cubi_app2()` (per impostare la larghezza della
  `shiny::selectInput()` inclusa in `dati2UI()`).  
- il valore predefinito dell'argomento choices della funzione `cubi_app2()` è
  stato modificato da scelte a lista_scelte.  


  

# cubiapp 0.0.3

- piccole modifiche (irrilevanti)




# cubiapp 0.0.2

- nuova "dipendenza": pacchetto `writexl` (per `writexl::write_xlsx()`, usata
  nel modulo "scarica_xlsx"; vedi poco sotto).  
- `tabellaServer()` e `tabella2Server()` adesso restituiscono anche la tabella
  filtrata e ridisposta come reactive (`tab_cast()`).  
- nuovo modulo "scarica": genera un `downloadButton()` per poter scaricare una
  tabella reactive (funzioni `scaricaUI()` e `scaricaServer()`) in csv o csv.gz.  
- nuovo modulo "scarica_xlsx": genera un `downloadButton()` per poter scaricare una
  tabella reactive (funzioni `scarica_xlsxUI()` e `scarica_xlsxServer()`) in xlsx.  
- Aggiunto a `cubi_app()` e `cubi_app2()` un bottone per scaricare la tabella
  filtrata e ridisposta (con i moduli "scarica" e "scarica_xlsx").  
- Aggiunto qualche argomento a `tabella2Server()`: valore_tabella (nome della
  colonna da usare nel `data.table::dcast()`, `value.var`) e ... (argomenti da
  passare a `shiny::renderTable()`).  
- `cubi_app2()`: nella chiamata a `tabella2Server()` sono stati aggiunti 
  `striped = TRUE, hover = TRUE, na = ""`.  




# cubiapp 0.0.1

- Il vettore character `scelte` adesso prende i nomi dei cubi direttamente dal 
pacchetto `cubustat`, mentre prima veniva generato manualmente. Questo viene 
fatto con `utils::data()` e quindi il pacchetto `utils` è stato aggiunto 
alla sezione Imports del file DESCRIPTION e importato nel NAMESPACE.  
- Aggiunto l'argomento `choices = scelte` alla funzione `cubi_app()`.  
- Documentazione e esportazione delle funzioni dei moduli `dati` (`datiUI()` e 
`datiServer`), `variabili` (`variabiliUI()` e `variabiliServer()`), `sposta` 
(`spostaUI()` e `spostaServer()`) e `tabella` (`tabellaOutput()` e 
`tabellaServer()`). Nota che questi moduli sono stati usati per creare l'app 
della funzione `cubi_app()`.  
- Documentazione e esportazione delle funzioni dei moduli `dati2` (`dati2UI()` 
e `dati2Server`), `variabili2` (`variabili2UI()` e `variabili2Server()`), 
`sposta2` (`sposta2UI()` e `sposta2Server()`) e `tabella2` (`tabella2Output()` 
e `tabella2Server()`). Questi moduli sono una versione simile a `dati`, 
`variabili`, `sposta` e `tabella`, con alcune elaborazioni ridondanti 
eliminate. Nota che questi moduli sono stati usati per creare l'app della 
funzione `cubi_app2()`.  


# cubiapp 0.0.0

- Unica funzione esportata nel NAMESPACE: `cubi_app()`. La funzione fa partire 
un'app shiny che permette di selezionare i cubi di dati che ci sono sul sito 
dell'Ustat (all'11.08.2020)  
- Internamente, il pacchetto contiene queste funzioni:  
    - `datiUI(id, choices = scelte)` / `datiServer(id)`  
    - `variabiliUI(id)` / `variabiliServer(id, dati = reactive(NULL))`  
    - `spostaUI(id)` / `spostaServer(id, dati = reactive(NULL))`  
    - `tabellaOutput(id)` / `tabellaServer(id, dati = reactive(NULL), lista_vars = reactive(NULL), formula = reactive(NULL))`  
- Nelle prossime versioni cercherò di documentare le funzioni dei moduli e di 
esportarle nel NAMESPACE  